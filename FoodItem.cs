﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PracticeHW_OOP
{
    public class FoodItem : IComparable<FoodItem>
    {
        public string m_meal { get; set; }
        public float m_price { get; set; }
        public bool _isEatOrDrink { get; set; }
        public double m_weight { get; set; }

        public FoodItem(string meal, float price, bool isEatOrDrink, double weight)
        {
            if (m_meal.Length < 5)
                throw new ArgumentException($"Sorry! meal name is wrong, {this}");
            m_meal = meal;
            if (m_price == 0)
                throw new ArgumentException($"Sorry! price cannot be Zero, {this}");
            if (m_price < 0)
                throw new NegativePriceException($"Sorry! price cannot be Negative, {this}");
            m_price = price;
            _isEatOrDrink = isEatOrDrink;
            if (m_weight == 0)
                throw new ArgumentException($"Sorry! weight cannot be Zero, {this}");
            if (m_weight < 0)
                throw new NegativeWeightException ($"Sorry! weight cannot be Negative, {this}");
            m_weight = weight;
        }

        public static bool operator ==(FoodItem meal1, FoodItem meal2)
        {
            if (meal1 is null && meal2 is null)
                return true;
            if (meal1 is null || meal2 is null)
                return false;
            return meal1.m_meal == meal2.m_meal;
        }
        public static bool operator !=(FoodItem meal1, FoodItem meal2)
        {
            return !(meal1.m_meal == meal2.m_meal);
        }

        public override bool Equals(object obj)
        {
            FoodItem foodItem = obj as FoodItem;

            return m_meal == foodItem.m_meal;

        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        public override string ToString()
        {
            return $"Meal : {m_meal}  Price : {m_price}  With Drink or Eat : {_isEatOrDrink}  Meal Weight : {m_weight}";
        }

        public int CompareTo(FoodItem other)
        {
            return -1 *(m_price.CompareTo(other.m_price)); //Price sort from Low to Hight
        }
    }
}
