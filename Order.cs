﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PracticeHW_OOP
{
    class Order
    {
        private readonly DateTime orderTime;
        
        private List<FoodItem> foodItems;

        public DateTime GetOrderTime // public properties starts with capital letter
        {
            get
            {
                return orderTime;
            }
        }
        public float Total
        {
            get
            {
                float count = 0;
                foreach (FoodItem item in foodItems)
                    count += item.m_price;
                return count;
            }
        }

        public DateTime ReceivedTime { get; set; }
        public string address { get; set; }

        public Order(List<FoodItem> foodItems, DateTime resiveOrder, string address)
        {
            this.orderTime = DateTime.Now;
            this.foodItems = foodItems;
            Console.WriteLine($"Order Time {orderTime} Resive Time {resiveOrder}");
            int minutes = int.Parse(Console.ReadLine());
            this.ReceivedTime = orderTime.AddMinutes(minutes);
            this.address = address;
        }

        public static Order operator +(FoodItem item, Order orders)
        {
            if (item == null)
                throw new ArgumentNullException("Must have a valid value");
            orders.foodItems.Add(new FoodItem (item.m_meal, item.m_price, item._isEatOrDrink, item.m_weight));
            return orders;
        }

        public static bool operator <(DateTime time, Order resive)
        {
            if ((time - resive.ReceivedTime).TotalMinutes == 0)
                return true;
            return false;
        }

        public static bool operator >(DateTime time, Order resive)
        {
            return !(time < resive);
        }

        public override string ToString()
        {
            return $"Order Information:\n  Order Time: {this.orderTime} Address: {this.address} resive order: {this.ReceivedTime}";
        }
    }
}
