﻿using System;
using System.Runtime.Serialization;

namespace PracticeHW_OOP
{
    [Serializable]
    internal class FoodItemNotRelevantException : Exception
    {
        public FoodItemNotRelevantException()
        {
        }

        public FoodItemNotRelevantException(string message) : base(message)
        {
        }

        public FoodItemNotRelevantException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected FoodItemNotRelevantException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}