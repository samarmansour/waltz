﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PracticeHW_OOP
{
    class FoodCatalog
    {
        private List<FoodItem> foodItems = new List<FoodItem>();
        public int m_count { get; private set; }
        public int m_countEat { get; private set; }
        public int m_drinkCount { get; private set; }

        public FoodCatalog(List<FoodItem> foodList)
        {
            this.foodItems = foodList; 
            m_count++;
            m_countEat++;
            m_drinkCount++;
        }

        public FoodItem this[string name]
        {
            get
            {
                foreach (FoodItem item in foodItems)
                {
                    if (item.m_meal.ToUpper() != name.ToUpper())
                       throw new FoodItemNotFoundException($"Sorry! we don't have {name} meal");
                }
                FoodItem result = foodItems.FirstOrDefault(e => e.m_meal.ToUpper() == name.ToUpper());
                return result;
            }
            internal set
            {
                int index = foodItems.FindIndex(e => e.m_meal.ToUpper() == name.ToUpper());
                foodItems[index].m_price = Convert.ToInt32(value);
            }
        }

        public List<float> this[int price]
        {
            get
            {
                List<float> foodItemPrice = new List<float>();
                foreach (FoodItem item in foodItems)
                {
                    foodItemPrice.Add(item.m_price);
                    foodItemPrice.Sort();
                }

                // Initialize difference as infinite 
                float temp = float.MaxValue;
                List<float> result = new List<float>();
                // Find the min diff by comparing difference 
                // of all possible pairs in given array 
                for (int i = 0; i < foodItemPrice.Count - 1; i++)
                    for (int j = i + 1; j < foodItemPrice.Count; j++)
                        if (Math.Abs((foodItemPrice[i] - foodItemPrice[j])) < temp)
                            temp = Math.Abs((foodItemPrice[i] - foodItemPrice[j]));
                            result.Add(temp);
                         
                return result;
            }
        }

        public static FoodCatalog operator +(FoodCatalog foodCatalog, FoodItem item1)
        {
            if (item1 == null)
                throw new ArgumentNullException("Must have a valid value");
            foodCatalog.foodItems.Add(new FoodItem(item1.m_meal, item1.m_price, item1._isEatOrDrink, item1.m_weight));
            return foodCatalog;
        }

        public static FoodCatalog operator -(FoodCatalog foodCatalog, FoodItem item1)
        {
            if (item1 == null)
                throw new ArgumentNullException("Must have a valid value");
            foodCatalog.foodItems.Remove(item1);
            return foodCatalog;
        }

    }
}
