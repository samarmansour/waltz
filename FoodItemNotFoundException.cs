﻿using System;
using System.Runtime.Serialization;

namespace PracticeHW_OOP
{
    [Serializable]
    internal class FoodItemNotFoundException : Exception
    {
        public FoodItemNotFoundException()
        {
        }

        public FoodItemNotFoundException(string message) : base(message)
        {
        }

        public FoodItemNotFoundException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected FoodItemNotFoundException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}