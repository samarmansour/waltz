﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PracticeHW_OOP
{
    class OrderBook
    {
        private List<Order> m_orders;
        private FoodCatalog m_foodCatalog;

        private FoodItem m_foodItem;

        public float totalPrice
        {
            get
            {
                float result = 0;
                foreach (Order item in m_orders)
                    result += item.Total;
                return result;
            }
        }

        public float totalNumOfOrders
        {
            get
            {
                return m_orders.Count;
            }
        }

        public OrderBook( FoodCatalog foodCatalog)
        {
            m_orders = new List<Order>();
            m_foodCatalog = foodCatalog;
        }

        public static OrderBook operator +(Order item, OrderBook orders)
        {
            if (item == null)
                throw new ArgumentNullException("Must have a valid value");
            if (!(orders.m_orders.Contains(item)))
                throw new FoodItemNotRelevantException("Sorry! your order is not on the menu");
            orders.m_orders.Add(item);
            return orders;
        }

        public static OrderBook operator -(OrderBook item, Order orders)
        {
            if (item == null)
                throw new ArgumentNullException("Must have a valid value");
            if ((orders.GetOrderTime - orders.ReceivedTime).TotalMinutes == 0)
                item.m_orders.Remove(orders);
            return item;
        }

        public List<Order> GetLate()
        {
            List<Order> lateOrders = new List<Order>();
            foreach (Order item in m_orders)
            {
                if (item.GetOrderTime.Minute - item.ReceivedTime.Minute > 30)
                {
                    lateOrders.Add(item);
                }
            }
            return lateOrders;
        }

        public List<Order> PackOnScooter(double weight)
        {
            double sumWeightInPack = 0;
            List<Order> orderWeight = new List<Order>();
            foreach (Order item in m_orders)
            {
                if (m_foodItem.m_weight < weight && sumWeightInPack < weight)
                {
                    orderWeight.Add(item);
                    sumWeightInPack +=  m_foodItem.m_weight;
                }
            }
            return orderWeight;
        }

        /// <summary>
        /// Need to return the X most expensive orders
        /// (assuming there are X. if not then the as much as possible)
        /// </summary>
        /// <param name="x"></param>
        /// <returns></returns>
        public List<Order> GetExpensive(int x)
        {
            // this is linq short solution
            // you can make a loop if you want
            // 1. sort the orders by price descending
            // 2. create an empty list
            // 3. add the first X itmes
            // but in linq - one line! :)
            return m_orders.OrderByDescending(o => o.Total).Take(x).ToList();
        }


        public Order this[string name]
        {
            get
            {
                Order result = m_orders.FirstOrDefault(e => m_foodItem.m_meal.ToUpper() == name.ToUpper());
                return result;
            }
        }
    }
}


